{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Funktionales Programmieren\n",
    "\n",
    "Bisher haben wir Python aus der Sicht des *imperativen* Programmier-Paradigmas gesehen. Insbesondere in der Naturwissenschaft ist jedoch eine *beschreibende* Sichtweise näher an der mathematischen Denkweise. Wir schauen uns hier einige der Konzepte an:\n",
    "\n",
    "- Funktionen im Sinne der mathematischen Definition: mathematische Funktionen sind Abbildung zwischen Mengen von möglichen Ein- und Ausgabewerten; insbesondere hängt der Wert nicht vom Kontext des \"Aufrufs\" ab, nur von den Argumenten (*referenzielle Transparenz*).\n",
    "- Mathematische *Werte* sind nicht *variabel*, im Gegensatz zu Python-Objekten, die *in-place* verändert werden können.\n",
    "- *Funktionale Programmierung* legt den Fokus auf unveränderliche Werte und referenziell transparente (\"reine\") Funktionen. Programme beschreiben den \"Fluss\" von Daten von der Erzeugung über diverse funktionale Transformationen bis hin zur Verarbeitung."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\"Functional programming combines the flexibility and power of abstract mathematics with the intuitive clarity of abstract mathematics.\"\n",
    "\n",
    "[![](https://imgs.xkcd.com/comics/functional.png)](http://xkcd.com/1270/)\n",
    "(Wir behandeln hier allerdings keine *tail recursion*)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Iteratoren und Generatoren\n",
    "\n",
    "### Iteratoren\n",
    "\n",
    "**Iteratoren** sind Objekte, die *iterierbar* sind, die also in `for`-Schleifen verwendet werden können, z.B.\n",
    "\n",
    "- Listen\n",
    "- Tupel\n",
    "- Dictionaries\n",
    "- ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "2\n",
      "3\n"
     ]
    }
   ],
   "source": [
    "liste = [1, 2, 3]\n",
    "for x in liste:\n",
    "    print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zusätzlich zu den in Python existierenden Iteratoren kann man auch auf flexible Weise eigene erzeugen. Die einfachste Möglichkeit dazu sind **Generatoren**.\n",
    "\n",
    "### Generatoren\n",
    "\n",
    "Generatoren sind Funktionen, die den **`yield`**-Befehl verwenden.\n",
    "\n",
    "``` python\n",
    "yield <Wert>\n",
    "```\n",
    "\n",
    "Generatoren verhalten sich anders als normale Funktionen:\n",
    "\n",
    "- Ihr Aufruf führt nicht direkt Code aus, sondern liefert ein **generator object** zurück. Dieses Objekt ist iterierbar.\n",
    "- Beim Iterieren wird der Code der Funktion ausgeführt, bis das erste Mal der `yield`-Befehl auftritt.\n",
    "- An dieser Stelle *hält die Funktion an* und gibt die Kontrolle mitsamt dem `<Wert>` an den Aufrufer (z.B. die `for`-Schleife) zurück.\n",
    "- Sobald das nächste Element angefordert wird (z.B. bei der nächsten Schleifen-Iteration), wird die Funktion *fortgesetzt* und läuft weiter bis zum nächsten `yield`.\n",
    "- Die Iteration wird beendet, wenn die Funktion zurückkehrt, entweder durch Erreichen des Code-Endes oder durch `return`.\n",
    "- `return`-Werte werden ignoriert.\n",
    "\n",
    "#### Beispiel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "r = <generator object myrange at 0x7f90a4684728>\n",
      "myrange start\n",
      "myrange yield 2\n",
      "Loop processing 2\n",
      "myrange yield 4\n",
      "Loop processing 4\n",
      "myrange yield 6\n",
      "Loop processing 6\n",
      "myrange yield 8\n",
      "Loop processing 8\n",
      "myrange yield 10\n",
      "Loop processing 10\n",
      "myrange end\n"
     ]
    }
   ],
   "source": [
    "def myrange(start, stop, step):\n",
    "    print('myrange start')\n",
    "    x = start\n",
    "    while x <= stop:\n",
    "        print('myrange yield {}'.format(x))\n",
    "        yield x\n",
    "        x = x + step\n",
    "    print('myrange end')\n",
    "\n",
    "r = myrange(2, 10, 2)\n",
    "print(\"r = {}\".format(r))\n",
    "\n",
    "for x in r:\n",
    "    print('Loop processing {}'.format(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** In den meisten Fällen sollten Generatoren keine *Effekte* haben, so wie `print` in dem Beispiel oben. Die Grundidee ist, das *Erzeugen* der Daten (Generator) zu trennen von der *Verarbeitung* (Loop); Effekte sollten üblicherweise nur bei der *Verarbeitung* auftreten.\n",
    "\n",
    "Generatoren müssen nicht zurückkehren. Es genügt, wenn der verarbeitende Loop an geeigneter Stelle abbricht. Der folgende Generator erzeugt der Reihe nach alle natürlichen Zahlen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "5\n",
      "6\n",
      "7\n"
     ]
    }
   ],
   "source": [
    "def numbers():\n",
    "    num = 0\n",
    "    while True:\n",
    "        yield num\n",
    "        num = num + 1\n",
    "\n",
    "for n in numbers():\n",
    "    print(n)\n",
    "    if n == 7:\n",
    "        break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Manchmal ist es nützlich, einen Iterator direkt vollständig abzuarbeiten und in eine Liste umzuwandeln. Dazu kann die `list()`-Funktion verwendet werden:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[3, 6, 9, 12]"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(range(3, 13, 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generator Expressions\n",
    "\n",
    "Analog zu *List Comprehensions* gibt es **Generator Expressions**. Der einzige syntaktische Unterschied ist, dass Generator Expressions mit runden Klammern geschrieben werden; sie erzeugen ihre Elemente erst beim Iterieren, während *List Comprehensions* direkt ausgeführt werden."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "--- List comprehension ---\n",
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "Ergebnis:\n",
      "[0, 1, 2, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "def print_and_return(x):\n",
    "    print(x)\n",
    "    return x\n",
    "\n",
    "print('--- List comprehension ---')\n",
    "liste = [print_and_return(x) for x in range(5)]\n",
    "print('Ergebnis:')\n",
    "print(liste)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "--- Generator expression ---\n",
      "Ergebnis:\n",
      "<generator object <genexpr> at 0x7f90a46848e0>\n"
     ]
    }
   ],
   "source": [
    "print('--- Generator expression ---')\n",
    "gen = (print_and_return(x) for x in range(5))\n",
    "print('Ergebnis:')\n",
    "print(gen)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "--- Iteration ---\n",
      "0\n",
      "Generator hat 0 geliefert.\n",
      "1\n",
      "Generator hat 1 geliefert.\n",
      "2\n",
      "Generator hat 2 geliefert.\n",
      "3\n",
      "Generator hat 3 geliefert.\n",
      "4\n",
      "Generator hat 4 geliefert.\n"
     ]
    }
   ],
   "source": [
    "print('--- Iteration ---')\n",
    "for x in gen:\n",
    "    print('Generator hat {} geliefert.'.format(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Funktionale Sichtweise auf Iteratoren\n",
    "\n",
    "Iteratoren stellen vom Standpunkt der funktionalen Programmierung einen \"Fluss\" von Eingabedaten dar, der durch Hilfs-Funktionen und Generator Expressions gefiltert und transformiert wird.\n",
    "\n",
    "Wichtig hierbei ist, dass im Gegensatz zur Verarbeitung von Listen dabei nicht in jedem Schritt eine vollständige neue Liste aufgebaut wird; stattdessen werden die einzelnen Verarbeitungsschritte zusammengefasst und erst am Ende (beim tatsächlichen Iterieren per Loop o.ä.) ausgeführt.\n",
    "\n",
    "Daher sollten die meisten durchgeführten Transformationen keine *Effekte* habe, also referenziell transparent sein, da nicht klar ist, zu welchem Zeitpunkt genau die Effekte tatsächlich ausgeführt werden.\n",
    "\n",
    "### Verändern und Weglassen von Elementen\n",
    "\n",
    "Um Elemente eines Iterators \"abzuändern\" oder aufgrund eines wie auch immer gearteten Kriteriums wegzulassen, kann man Generator Expressions wie\n",
    "```python\n",
    "(f(x) for x in iterable if condition(x))\n",
    "```\n",
    "verwenden.\n",
    "\n",
    "**Beispiel:** Quadrate aller durch 4 teilbaren Zahlen zwischen 1 und 20."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<generator object <genexpr> at 0x7f90a4684780>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l = (x**2 for x in range(1, 21) if x % 4 == 0)\n",
    "l"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Da es sich um einen Iterator handelt, muss man ihn erst *benutzen* um tatsächliche Auswertungen zu erhalten. Das nennt man auch *lazy evaluation*, die Verzögerung der Auswertung bis die Werte benötigt werden."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[16, 64, 144, 256, 400]"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(l)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Das Modul `itertools`\n",
    "\n",
    "`itertools` enthält eine Reihe von Hilfs-Funktionen zum Arbeiten mit Iteratoren.\n",
    "\n",
    "#### `count`\n",
    "```python\n",
    "itertools.count(start=0, step=1)\n",
    "```\n",
    "funktioniert wie die oben definierte Funktion `numbers`: `count` liefert einen Iterator über die Zahlen `(start, start + step, start + 2*step, ...)`\n",
    "\n",
    "#### `takewhile`, `dropwhile`\n",
    "```python\n",
    "itertools.takewhile(condition, iterable)\n",
    "itertools.dropwhile(condition, iterable)\n",
    "```\n",
    "`takewhile` liefert Elemente aus `iterable`, solange die `condition`-Funktion *wahr* zurückgibt, und bricht beim ersten *falschen* Wert ab. `dropwhile` überspringt die entsprechenden Elemente am Anfang und liefert nur den Rest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 4, 9, 16, 25, 36, 49]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from itertools import takewhile, count\n",
    "list(takewhile(lambda x: x < 50, (x**2 for x in count())))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Achtung:** Würde man hier eine List Comprehension für die Liste aller (!) Quadratzahlen verwenden, würde das Programm nicht beenden, trotz `takewhile`.\n",
    "\n",
    "#### `repeat`\n",
    "```python\n",
    "itertools.repeat(x)\n",
    "```\n",
    "erzeugt einen Iterator, der (potentiell) unendlich oft `x` zurückliefert. `repeat(x, n)` liefert `n`-mal `x`.\n",
    "\n",
    "#### `cycle`\n",
    "```python\n",
    "itertools.cycle(iterable)\n",
    "```\n",
    "erzeugt einen Iterator, der die übergebene `iterabel` immer wieder durchläuft.\n",
    "\n",
    "#### `chain`\n",
    "```python\n",
    "itertools.chain(iterable1, iterable2, ...)\n",
    "```\n",
    "erzeugt einen Iterator, der die übergebenen `iterables` der Reihe nach durchläuft.\n",
    "\n",
    "#### `zip`\n",
    "```python\n",
    "zip(iterable1, iterable2, ...)\n",
    "```\n",
    "erzeugt einen Iterator, dessen Elemente Tupel sind. Jedes Tupel enthält je ein Element aus jeder `iterable`. Sobald eine der `iterables` abbricht, bricht auch die gesamte Iteration ab.\n",
    "\n",
    "Z.B. ist `zip(count(), liste)` äquivalent zu `enumerate(liste)`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[(0, 'a'), (1, 'b'), (2, 'c'), (3, 'd'), (4, 'e'), (5, 'f')]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(zip(count(), 'abcdef'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "for idx, char in zip(count(),'abcdef'):\n",
    "    idx, char\n",
    "    \n",
    "a = 'abcdef'\n",
    "b = range(len(a))\n",
    "for i in range(len(a)):\n",
    "    b[i], a[i]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `islice`\n",
    "```ipython\n",
    "itertools.islice(iterable, start, stop, step)\n",
    "```\n",
    "nimmt eine Teilmenge der Elemente eines Iterators. `start`, `stop` und `step` funktionieren analog zur `range`-Funktion. `start` und `step` können auch weggelassen werden."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from itertools import islice\n",
    "list(islice((x**2 for x in count()), 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Verarbeitung\n",
    "\n",
    "Iteratoren müssen nach ihrer Erzeugung und eventuellen Transformationen wie oben verarbeitet werden, damit die Operationen tatsächlich ausgeführt werden. Dabei wird der Iterator, der eine *potentielle* Berechnung darstellt, zu einem *tatsächlichen* Wert reduziert oder eine *tatsächliche* Operation mit den Elementen durchgeführt.\n",
    "\n",
    "Die allgemeinste Verarbeitung ist einfach eine `for`-Schleife:\n",
    "```ipython\n",
    "for x in iterable:\n",
    "    do_something_with(x)\n",
    "```\n",
    "\n",
    "Für häufige Spezialfälle gibt es auch vordefinierte Funktionen.\n",
    "\n",
    "#### `list`\n",
    "\n",
    "... wurde oben schon verwendet: der Iterator wird in eine Liste umgewandelt, die alle Elemente enthält.\n",
    "\n",
    "#### `dict`\n",
    "\n",
    "... wandelt einen Iterator, der Tupel erzeugt, in ein Dictionary um.\n",
    "\n",
    "#### `sum`, `max`, `min`\n",
    "\n",
    "... berechnen die Summe, Maximum bzw. Minimum aller Elemente.\n",
    "\n",
    "#### `any`, `all`\n",
    "\n",
    "... testen, ob irgendein bzw. alle Elemente des Iterators *wahr* sind.\n",
    "\n",
    "## Memoization\n",
    "\n",
    "Ein Vorteil referenziell transparenter Funktionen ist, dass sie\n",
    "- bei jedem Aufruf mit gleichen Argumenten das gleiche Ergebnis liefern, und\n",
    "- ausschließlich aus diesem Grund aufgerufen werden, also außer dem Rückgabewert nichts Anderes produzieren.\n",
    "\n",
    "D.h. dass man, nachdem die Funktion einmal aufgerufen wurde, im Prinzip das Ergebnis speichern und für weitere Aufrufe verwenden kann. Das nennt man *Memoization*.\n",
    "\n",
    "Python macht so etwas standardmäßig nicht, da es nicht entscheiden kann, welche Funktionen \"rein\" sind, und da Memoization nicht für alle Funktionen sinnvoll ist (zum Beispiel wird der Speicherverbauch erhöht).\n",
    "\n",
    "Das Module `functools` enthält einen Decorator `lru_cache` mit dem Funktionen selektiv memoisiert werden können.\n",
    "```python\n",
    "functools.lru_cache(maxsize=None)\n",
    "```\n",
    "`maxsize` legt die Anzahl der gespeicherten Werte fest. Wird diese überschritten werden die memoisierten Argumente gelöscht, deren Aufruf am längsten zurückliegt (\"least recently used\").\n",
    "\n",
    "#### Beispiel: Fibonacci (schon wieder)\n",
    "\n",
    "Erinnerung: die naive Implementierung der Fibonacci-Folge"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fib(n):\n",
    "    if n <= 1:\n",
    "        return n\n",
    "    else:\n",
    "        return fib(n - 1) + fib(n - 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ist extrem langsam, da viele der Folgenglieder mehrfach berechnet werden müssen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "464 ms ± 6.14 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)\n"
     ]
    }
   ],
   "source": [
    "%timeit fib(30)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mit `lru_cache` (der Einfachheit halber ohne `maxsize`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from functools import lru_cache\n",
    "\n",
    "@lru_cache(maxsize=None)\n",
    "def fib(n):\n",
    "    if n <= 1:\n",
    "        return n\n",
    "    else:\n",
    "        return fib(n - 1) + fib(n - 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "143 ns ± 2.69 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit fib(30)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Higher order functions\n",
    "\n",
    "Unter *higher order functions* versteht man Funktionen, die Funktionen verarbeiten und wieder Funktionen zurückliefern.\n",
    "\n",
    "### Decorators\n",
    "\n",
    "sind *syntactic sugar*, um die Definition von Funktionen zu beeinflussen. \n",
    "\n",
    "```python\n",
    "@some_decorator\n",
    "def some_function(arg1, arg2, ...):\n",
    "    statement1\n",
    "    statement2\n",
    "    ...\n",
    "```\n",
    "\n",
    "ist äquivalent zu\n",
    "\n",
    "```python\n",
    "def some_function(arg1, arg2, ...):\n",
    "    statement1\n",
    "    statement2\n",
    "    ...\n",
    "    \n",
    "some_function = some_decorator(some_function)\n",
    "```\n",
    "\n",
    "D.h. nach der ersten Definition von `some_function` wird `some_decorator` ausgeführt und kann eine beliebige *neue Definition* der Funktion zurückgeben.\n",
    "\n",
    "*Beispiel*: Decorator, der Funktionsargumente und Rückgabewerte ausgibt (z.B. zum Debugging):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_args_and_result(func):\n",
    "    def new_func(*args):\n",
    "        print(\"-- {} called with arguments {}\".format(func.__name__, args))\n",
    "        result = func(*args)\n",
    "        print(\"-- {} returned {}\".format(func.__name__, result))\n",
    "        return result\n",
    "    return new_func"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hierbei ist\n",
    "```python\n",
    "*args\n",
    "```\n",
    "die Liste aller Argumente der Funktion `func`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-- print_sum called with arguments (3, 4)\n",
      "7\n",
      "-- print_sum returned 7\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "@print_args_and_result\n",
    "def print_sum(a, b):\n",
    "    \"\"\"\n",
    "    Dokumentation...\n",
    "    \"\"\"\n",
    "    c = a + b\n",
    "    print(c)\n",
    "    return c\n",
    "\n",
    "print_sum(3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `functools.wraps`\n",
    "\n",
    "Die neue Funktion übernimmt nicht die Metadaten der ursprünglichen, z.B. Funktionsname oder Docstring:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'new_func'"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "print_sum.__name__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "None\n"
     ]
    }
   ],
   "source": [
    "print(print_sum.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Um das zu erreichen, empfiehlt es sich, die neue Funktion selbst mit `functools.wraps` zu dekorieren:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from functools import wraps\n",
    "\n",
    "def print_args_and_result(func):\n",
    "    @wraps(func)\n",
    "    def new_func(*args):\n",
    "        print(\"-- {} called with arguments {}\".format(func.__name__, args))\n",
    "        result = func(*args)\n",
    "        print(\"-- {} returned {}\".format(func.__name__, result))\n",
    "        return result\n",
    "    return new_func"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-- print_sum called with arguments (3, 4)\n",
      "7\n",
      "-- print_sum returned 7\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "@print_args_and_result\n",
    "def print_sum(a, b):\n",
    "    \"\"\"\n",
    "    Calculate the sum of two numbers, print and return the result.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    a, b: number\n",
    "        Numbers to add\n",
    "      \n",
    "    Returns\n",
    "    -------\n",
    "    number:\n",
    "        The sum of a and b.\n",
    "    \"\"\"\n",
    "    c = a + b\n",
    "    print(c)\n",
    "    return c\n",
    "\n",
    "print_sum(3, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'print_sum'"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "print_sum.__name__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "    Calculate the sum of two numbers, print and return the result.\n",
      "    \n",
      "    Parameters\n",
      "    ----------\n",
      "    a, b: number\n",
      "        Numbers to add\n",
      "      \n",
      "    Returns\n",
      "    -------\n",
      "    number:\n",
      "        The sum of a and b.\n",
      "    \n"
     ]
    }
   ],
   "source": [
    "print(print_sum.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Partielle Auswertung von Funktionen\n",
    "\n",
    "Ebenfalls im Sinne der funktionalen Programmierung ist das partielle Auswerten von Funktionen; Dabei werden Teile der Argumente einer gegebenen Funktion gesetzt und man erhält wiederum eine Funktion. Genau genommen funktioniert das mit allen ausführbaren (callable) Objekten.\n",
    "\n",
    "Insbesondere ist das nützlich um Code wieder zu verwenden und eine einfache Weiterverarbeitung von Funktionen zu ermöglichen.\n",
    "\n",
    "Bei Funktionen die Algorithmen implementieren werden häufig viele Parameter gesetzt und partial ist auch eine Möglichkeit Klassen von Problemen zu definieren. Beispiele dazu sehen wir später.\n",
    "\n",
    "*Beispiel:* Potenzieren"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from functools import partial"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def power(base, exponent):\n",
    "    return base ** exponent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir setzen den Exponenten zu 2 und 3 und erhalten so 2 spezielle Funktionen"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cube = partial(power, exponent=3)\n",
    "square = partial(power, exponent=2)\n",
    "\n",
    "square(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alle Argumente können gesetzt werden, auch welche die keine *named arguments* sind. Allerdings kann man dann die basis Argumente nicht mehr setzen; das gibt einen Fehler, weil versucht wurde, diesen doppelt zu setzen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "power() got multiple values for argument 'base'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m--------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                          Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-29-a3b94d8524cc>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0mpotenziere_vier\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mpartial\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mpower\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0mbase\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m4\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      2\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m \u001b[0mpotenziere_vier\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m3\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: power() got multiple values for argument 'base'"
     ]
    }
   ],
   "source": [
    "potenziere_vier = partial(power,base=4)\n",
    "\n",
    "potenziere_vier(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "16"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "potenziere_vier(exponent=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Man bekommt auch die gesetzten Parameter der Funktion nach der Anwendung `partial`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'exponent': 2}"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "square.keywords "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  },
  "name": "07_Funktionales Programmieren.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
